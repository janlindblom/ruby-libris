module Libris
  module Data
    class Author
      attr_reader :name, :born, :alive, :deceased

      def initialize(text=nil)
        # @todo: expected text format: "Lastname, Firstname BORN-[DEAD]"
      end

      private

      def set_alive(alive=true)
        @alive = alive
      end

      def set_name(author_name="")
        @name = author_name
      end

      def set_born(author_born=nil)
        @born = author_born
      end

      def set_deceased(author_deceased=nil)
        @deceased = author_deceased
      end
    end
  end
end